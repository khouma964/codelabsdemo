<?php
use App\Http\Controllers\StoriesController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/story', 'StoriesController@index')->name('story.list');
    Route::get('/story/view/{id}', [StoriesController::class, 'view'])->name('story.view');
    Route::get('/story/add', [StoriesController::class, 'create'])->name('story.add');
    Route::post('/story/save', [StoriesController::class, 'store'])->name('story.save');
});
